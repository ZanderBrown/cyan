#!/usr/bin/python3
from __future__ import annotations

import logging
import sys
from typing import Optional

import gi

gi.require_version("Gst", "1.0")
from gi.repository import Gio, GLib, Gst

Gst.init(None)


logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)7s: %(message)s",
    datefmt="%H:%M:%S",
)
log = logging.getLogger(__name__)

loop = GLib.MainLoop()


class ScreenCast:
    BUS_NAME = "org.gnome.Mutter.ScreenCast"
    BUS_PATH = "/org/gnome/Mutter/ScreenCast"
    INTERFACE = "org.gnome.Mutter.ScreenCast"

    def __init__(self, bus: Gio.DBusConnection) -> None:
        self.bus = bus

    def create_session(self) -> ScreenCastSession:
        reply = self.bus.call_sync(
            self.BUS_NAME,
            self.BUS_PATH,
            self.INTERFACE,
            "CreateSession",
            GLib.Variant("(a{sv})", ([],)),
            GLib.VariantType("(o)"),
            Gio.DBusCallFlags.NONE,
            -1,
            None,
        )
        (path,) = reply.unpack()
        return ScreenCastSession(self.bus, path)


class ScreenCastSession:
    BUS_NAME = "org.gnome.Mutter.ScreenCast"
    INTERFACE = "org.gnome.Mutter.ScreenCast.Session"

    def __init__(self, bus: Gio.DBusConnection, path: str) -> None:
        self.bus = bus
        self.path = path
        log.info("Session at %s", self.path)

    def start(self) -> None:
        self.bus.call_sync(
            self.BUS_NAME,
            self.path,
            self.INTERFACE,
            "Start",
            None,
            None,
            Gio.DBusCallFlags.NONE,
            -1,
            None,
        )

    def stop(self) -> None:
        self.bus.call_sync(
            self.BUS_NAME,
            self.path,
            self.INTERFACE,
            "Stop",
            None,
            None,
            Gio.DBusCallFlags.NONE,
            -1,
            None,
        )

    def record_monitor(self, connector: str) -> ScreenCastStream:
        reply = self.bus.call_sync(
            self.BUS_NAME,
            self.path,
            self.INTERFACE,
            "RecordMonitor",
            GLib.Variant(
                "(sa{sv})", (connector, {"cursor-mode": GLib.Variant("u", 1)})
            ),
            GLib.VariantType("(o)"),
            Gio.DBusCallFlags.NONE,
            -1,
            None,
        )
        (path,) = reply.unpack()
        return ScreenCastStream(self.bus, path)

    def record_window(self) -> ScreenCastStream:
        reply = self.bus.call_sync(
            self.BUS_NAME,
            self.path,
            self.INTERFACE,
            "RecordMonitor",
            GLib.Variant("(a{sv})", ({"cursor-mode": GLib.Variant("u", 1)},)),
            GLib.VariantType("(o)"),
            Gio.DBusCallFlags.NONE,
            -1,
            None,
        )
        (path,) = reply.unpack()
        return ScreenCastStream(self.bus, path)

    def record_area(self, x: int, y: int, w: int, h: int) -> ScreenCastStream:
        reply = self.bus.call_sync(
            self.BUS_NAME,
            self.path,
            self.INTERFACE,
            "RecordMonitor",
            GLib.Variant(
                "(iiiia{sv})", (x, y, w, h, {"cursor-mode": GLib.Variant("u", 1)})
            ),
            GLib.VariantType("(o)"),
            Gio.DBusCallFlags.NONE,
            -1,
            None,
        )
        (path,) = reply.unpack()
        return ScreenCastStream(self.bus, path)


class ScreenCastStream:
    BUS_NAME = "org.gnome.Mutter.ScreenCast"
    INTERFACE = "org.gnome.Mutter.ScreenCast.Stream"

    pipeline: Optional[Gst.Element]

    def __init__(self, bus: Gio.DBusConnection, path: str) -> None:
        self.bus = bus
        self.path = path
        log.info("Stream at %s", self.path)

        self.subscription = self.bus.signal_subscribe(
            self.BUS_NAME,
            self.INTERFACE,
            "PipeWireStreamAdded",
            self.path,
            None,
            Gio.DBusSignalFlags.NONE,
            self.on_pipewire,
        )

    def on_pipewire(
        self,
        bus: Gio.DBusConnection,
        sender: str,
        path: str,
        interface: str,
        signal: str,
        parameters: GLib.Variant,
    ) -> None:
        (node_id,) = parameters.unpack()

        log.info("added")

        # vaapi pipeline running completely on hw encoder, perfect...
        #    pipeline = Gst.parse_launch('pipewiresrc path=%u do-timestamp=true keepalive-time=1000 resend-last=true ! video/x-raw,max-framerate=599588127136230/10000000000000 ! vaapipostproc ! vaapih264enc tune=3 quality-level=7 ! queue ! h264parse ! mp4mux ! filesink location=recording.mp4'%node_id)

        # vp9 is slow as fuck
        #    pipeline = Gst.parse_launch('pipewiresrc path=%u do-timestamp=true keepalive-time=1000 resend-last=true ! video/x-raw,max-framerate=30/1 ! videoconvert ! queue ! vp9enc min_quantizer=13 max_quantizer=13 cpu-used=16 deadline=1 threads=4 ! queue ! webmmux ! filesink location=recording.webm'%node_id)

        # vp8 is also slow
        # videoconvert here is optimized for our case
        self.pipeline = Gst.parse_launch(
            "pipewiresrc path=%u do-timestamp=true keepalive-time=1000 resend-last=true ! video/x-raw,max-framerate=30/1 ! videoconvert chroma-mode=GST_VIDEO_CHROMA_MODE_NONE dither=GST_VIDEO_DITHER_NONE matrix-mode=GST_VIDEO_MATRIX_MODE_OUTPUT_ONLY n-threads=4 ! queue ! vp8enc end-usage=cq cq-level=13 deadline=1 threads=4 ! queue ! webmmux ! filesink location=recording.webm"
            % node_id
        )

        # rav1e at 60fps
        #    pipeline = Gst.parse_launch('pipewiresrc path=%u do-timestamp=true keepalive-time=1000 resend-last=true ! video/x-raw,max-framerate=60/1 ! videoconvert n-threads=16 ! queue ! rav1enc threads=16 speed-preset=10 ! queue ! webmmux ! filesink location=recording_av1.webm'%node_id)

        # x264 the gold standard of sw encoding, can handle 1080p at 60fps
        #    pipeline = Gst.parse_launch('pipewiresrc path=%u do-timestamp=true keepalive-time=1000 resend-last=true ! video/x-raw,max-framerate=30/1 ! videoconvert chroma-mode=GST_VIDEO_CHROMA_MODE_NONE dither=GST_VIDEO_DITHER_NONE matrix-mode=GST_VIDEO_MATRIX_MODE_OUTPUT_ONLY n-threads=4 ! queue ! x264enc quantizer=0 speed-preset=ultrafast pass=quant ! queue ! h264parse ! mp4mux ! filesink location=recording.mp4'%node_id)

        # openh264, second best sw encoder, can also handle 1080p at 60fps pretty well
        # options carefully selected for best performance, usage-type=screen was deliberately not choosen as it's a lot slower
        #    pipeline = Gst.parse_launch('pipewiresrc path=%u do-timestamp=true keepalive-time=1000 resend-last=true ! video/x-raw,max-framerate=30/1 ! videoconvert chroma-mode=GST_VIDEO_CHROMA_MODE_NONE dither=GST_VIDEO_DITHER_NONE matrix-mode=GST_VIDEO_MATRIX_MODE_OUTPUT_ONLY n-threads=4 ! queue ! openh264enc deblocking=off background-detection=false complexity=low adaptive-quantization=false qp-max=26 qp-min=26 multi-thread=4 slice-mode=auto ! queue ! h264parse ! mp4mux ! filesink location=recording.mp4'%node_id)

        # x264 with color conversion on the gpu, sadly not as fast as one would hope
        #    pipeline = Gst.parse_launch('pipewiresrc path=%u do-timestamp=true ! video/x-raw,max-framerate=30/1 ! glupload ! glcolorconvert ! gldownload ! queue ! x264enc quantizer=0 speed-preset=ultrafast pass=quant ! queue ! h264parse ! mp4mux ! filesink location=recording.mp4'%node_id)

        self.pipeline.set_state(Gst.State.PLAYING)
        gst_bus = self.pipeline.get_bus()
        gst_bus.add_signal_watch()
        gst_bus.connect("message", self.on_message)
        log.info("new pipeline: %s", self.pipeline)

    def terminate(self) -> None:
        log.info("pipeline: %s", self.pipeline)
        if self.pipeline:
            log.info("sending eos")
            self.pipeline.send_event(Gst.Event.new_eos())

    def on_message(self, bus: Gst.Bus, message: Gst.Message) -> None:
        assert self.pipeline is not None
        t = message.type
        log.info("message pipeline: %s", t)
        if t == Gst.MessageType.EOS or t == Gst.MessageType.ERROR:
            log.info("I RUN!!!")
            self.pipeline.set_state(Gst.State.NULL)
            session.stop()
            loop.quit()


bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
screen_cast = ScreenCast(bus)
session = screen_cast.create_session()

if len(sys.argv) == 6 and sys.argv[1] == "-a":
    [_, _, x, y, width, height] = sys.argv
    stream = session.record_area(int(x), int(y), int(width), int(height))
elif len(sys.argv) == 2 and sys.argv[1] == "-w":
    stream = session.record_window()
else:
    stream = session.record_monitor("")

session.start()

try:
    loop.run()
except KeyboardInterrupt:
    log.info("interrupted")
    stream.terminate()
    loop.run()
